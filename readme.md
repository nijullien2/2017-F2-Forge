# Cours Forge ZZ2 F2 2017

# Infos étudiant :

Par binôme Nom Prénom:
 * Nom Prénom
 * Nom Prénom



## Consignes générales
### Format du document

Ce TP est et son compte rendu est un fichier au format *md* pour **Markdown** qui est un format permettant de générer facilement du HTML. Ce format étant couramment supporté par les outils de développement, nous l'utiliserons pour les comptes rendus.

Plus d'information sur la syntaxe de ce format :
* Spécification : https://daringfireball.net/projects/markdown/
* Cheatsheet : https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet
* Version GitLab : https://docs.gitlab.com/ee/user/markdown.html#code-and-syntax-highlighting

Ci-dessous un exemple d’éditeur à utiliser pour obtenir une prés visualisation de votre document :
* **Atom** avec le plugin **markdown-preview**
  * ctrl-shift-M pour afficher la preview
* **Notepad++** avec le plugin **MarkdownViewerPlusPlus**
* **SublimeText** avec le plugin **sublimetext-markdown-preview**
* **Eclipse**
* **IntelliJ**
* **vi / blocnote**
* ...


### Exemple

Compléter le document avec vos réponses, pour chaque consignes indiquez les commandes utilisées, le résultat et un commentaire. Par exemple :

> #### 1. Création d'un répertoire, ajout d'un fichier et lister le répertoire
> ##### Listing des commandes et résultat
    user@localhost:~/$ mkdir temp
    user@localhost:~/$ cd temp/
    user@localhost:~/temp$ ls
    user@localhost:~/temp$ touch newFile
    user@localhost:~/temp$ ls
    newFile
> ##### Commentaire
> Création d'un nouveau dossier **temp** dans la répertoire courant et ajout d'un fichier **newFile**, nous avons utiliser la commande `ls` pour lister le contenu de ce nouveau répertoire.

* Snipplet de code

```java
  // ce ci est un commentaire
  public String test = "Salut"
```

les cours sont disponibles [ici](2017ISIMA.pptx)
