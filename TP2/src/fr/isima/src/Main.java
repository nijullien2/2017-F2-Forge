package fr.isima.src;


public class Main {

	public static void main(String[] args) {
		
		if (args.length == 0) {
			System.err.println("Usage: java -jar bidule kilometrage");
			return;
		}
		
		double km = Double.parseDouble(args[0]);
		
		System.out.println("Montant : " + getPrice(km));

	}
	
	public static double getPrice(double km) {
		
		if (km < 0) throw new IllegalArgumentException();
		
		double price;
		if (km < 10) {
			price = 1.50 * km;
		} else if (km < 40) {
			price = 0.40 * km;
		} else if (km < 60) {
			price = 0.55 * km;
		} else {
			price = 6.81 * ((int) km / 20);
		}
		return price;
	}

}
