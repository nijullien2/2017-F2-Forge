package fr.isima.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import fr.isima.src.Main;

public class Price {

	@Test(expected = IllegalArgumentException.class)
	public void negativePrice() {
		double r = Main.getPrice(-1.d);
		System.out.println(r);
	}

	@Test
	public void price0() {
		double r = Main.getPrice(0.d);
		assertEquals(r, 0.d, 0);
	}
	
	@Test
	public void priceInf10() {
		double r = Main.getPrice(5.d);
		assertEquals(r, 1.50 * 5.d, 0);
	}
	
	@Test
	public void price10() {
		double r = Main.getPrice(10.d);
		assertEquals(r, 10.d * 0.40, 0);
	}
	
	@Test
	public void priceInf40() {
		double r = Main.getPrice(25.d);
		assertEquals(r, 0.40 * 25.d, 0);
	}

	@Test
	public void price40() {
		double r = Main.getPrice(40.d);
		assertEquals(r, 40.d * 0.55, 0);
	}
	
	@Test
	public void priceInf60() {
		double r = Main.getPrice(45.d);
		assertEquals(r, 0.55 * 45.d, 0);
	}

	@Test
	public void price60() {
		double r = Main.getPrice(60.d);
		assertEquals(r, 6.81 * 3.0, 0);
	}
	
	@Test
	public void priceSup60() {
		double r = Main.getPrice(70.d);
		assertEquals(r, 6.81 * 3.0, 0);
	}
	
	@Test
	public void priceSup60Bis() {
		double r = Main.getPrice(135.d);
		assertEquals(r, 6.81 * 6.0, 0);
	}
	
	@Test
	public void priceSup60Ter() {
		double r = Main.getPrice(215.d);
		assertEquals(r, 6.81 * 10.0, 0);
	}
	
	@Test
	public void priceSup60Quar() {
		double r = Main.getPrice(80.d);
		assertEquals(r, 6.81 * 4.0, 0);
	}
	
	@Test(expected = NumberFormatException.class)
	public void stringInput() {
		String[] args = {"test"};
		Main.main(args);
	}

}
